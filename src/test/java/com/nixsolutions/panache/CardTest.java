package com.nixsolutions.panache;

import com.nixsolutions.panache.entity.Card;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.WebApplicationException;

@QuarkusTest
public class CardTest {

    public static final long CARD_ID = 12L;
    public static final long CARD_AMOUNT = 42L;
    public static final long DEFAULT_CARD_AMOUNT = 0L;

    @Test
    public void testPanacheMocking() {
        PanacheMock.mock(Card.class);

        // Mocked classes always return a default value
        Assertions.assertEquals(DEFAULT_CARD_AMOUNT, Card.count());

        // Now let's change the return value
        Mockito.when(Card.count()).thenReturn(CARD_AMOUNT);
        Assertions.assertEquals(CARD_AMOUNT, Card.count());

        // Now let's call the original method
        Mockito.when(Card.count()).thenCallRealMethod();
        Assertions.assertEquals(DEFAULT_CARD_AMOUNT, Card.count());

        // Check that we called it 3 times
        PanacheMock.verify(Card.class, Mockito.times(3)).count();

        // Mock only with specific parameters
        Card card = new Card();
        Mockito.when(Card.findById(CARD_ID)).thenReturn(card);
        Assertions.assertSame(card, Card.findById(CARD_ID));
        Assertions.assertNull(Card.findById(CARD_AMOUNT));

        // Mock throwing
        Mockito.when(Card.findById(CARD_ID)).thenThrow(new WebApplicationException());
        Assertions.assertThrows(WebApplicationException.class, () -> Card.findById(CARD_ID));

    }
}
