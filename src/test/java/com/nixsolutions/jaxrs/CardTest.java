package com.nixsolutions.jaxrs;

import com.nixsolutions.jaxrs.entity.JaxrsCard;
import com.nixsolutions.jaxrs.service.CardService;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class CardTest {

    public static final Long CARD_ID = 1L;
    public static final Long NUMBER_OF_TRANSISTORS = 500000000L;
    public static final String AMOUNT_OF_MEMORY = "8GB";
    public static final String NAME_OF_CARD = "Super Card";
    public static final Long NUMBER_OF_CORES = 256L;
    public static final String ExpectedResponse = "{\"id\":1,\"name\":\"Super Card\",\"memory\":\"8GB\",\"cores\":256,\"transistors\":500000000}";
    public static JaxrsCard payload;


    @InjectMock
    CardService cardService;

    @BeforeAll
    public static void prepareCard() {
        payload = new JaxrsCard(CARD_ID,
                NAME_OF_CARD,
                AMOUNT_OF_MEMORY,
                NUMBER_OF_CORES,
                NUMBER_OF_TRANSISTORS);
    }

    @Test
    public void testFindCardById() {
        Mockito.when(cardService.getCard(CARD_ID))
                .thenReturn(payload);
        given()
                .when().get("/jaxrs/card/{id}", CARD_ID)
                .then()
                .statusCode(200)
                .contentType("application/json")
                .body(is(ExpectedResponse));
    }

    @Test
    public void testGreeting() {
        given()
                .when().get("/jaxrs/card/greeting")
                .then()
                .statusCode(200)
                .body(is("Hello Quarkus"));
    }

}