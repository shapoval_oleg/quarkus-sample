package com.nixsolutions.jaxrs.resource;

import com.nixsolutions.jaxrs.entity.JaxrsCard;
import com.nixsolutions.jaxrs.service.CardService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Random;

@Path("/jaxrs/card")
public class CardResource {

    @Inject
    CardService cardService;

    private final Random random = new Random();

    @Transactional
    @POST
    @Consumes("application/json")
    public Response createCard(JaxrsCard card) {
        cardService.createCard(card);
        return Response.ok().build();
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response getCard(@PathParam("id") Long id) {
        return Response.ok(cardService.getCard(id)).build();
    }

    @Transactional
    @PUT
    @Consumes("application/json")
    public Response updateCard(JaxrsCard card) {
        cardService.updateCard(card);
        return Response.ok().build();
    }

    @Transactional
    @DELETE
    @Path("{id}")
    public Response deleteCard(@PathParam("id") Long id) {
        cardService.deleteCard(id);
        return Response.ok().build();
    }

    @GET
    @Path("/greeting")
    @Produces("text/plain")
    public Response greeting() {
        return Response.ok("Hello Quarkus").build();
    }
}