package com.nixsolutions.jaxrs.service;


import com.nixsolutions.jaxrs.entity.JaxrsCard;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@ApplicationScoped
public class CardService {

    @Inject
    EntityManager entityManager;

    @Transactional
    public void createCard(JaxrsCard card) {
        entityManager.persist(card);
    }

    @Transactional
    public JaxrsCard getCard(Long id) {
        return entityManager.find(JaxrsCard.class, id);
    }

    @Transactional
    public void updateCard(JaxrsCard card) {
        createCard(card);
    }

    @Transactional
    public void deleteCard(Long id) {
        entityManager.remove(new JaxrsCard(id));
    }

    public String greeting() {
        return "Hello Quarkus";
    }
}
