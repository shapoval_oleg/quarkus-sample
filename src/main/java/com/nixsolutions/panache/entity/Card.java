package com.nixsolutions.panache.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Card extends PanacheEntity {

    public String name;
    public String memory;
    public Long cores;
    public Long transistors;

    public Card findByName(String name) {
        return find("name",name).firstResult();
    }

}
