package com.nixsolutions.panache.resource;

import com.nixsolutions.panache.entity.Card;
import io.quarkus.hibernate.orm.rest.data.panache.PanacheEntityResource;

public interface CardResource  extends PanacheEntityResource<Card,Long> {}